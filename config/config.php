<?php

//define("BASE_URL","http://fundamentamosalimentos.co/");
const BASE_URL ="http://fundamentamosalimentos.net.co";

//ZONA HORARIA
date_default_timezone_set('America/Bogota');

//BASE DE DATOS
const DB_HOST = "localhost:3307";
const DB_NAME = "db_fasas";
const DB_USER = "root";
const DB_PASSW = "";
const DB_CHARSET = "charset=utf8mb4";

//Deliminadores decimal y millar Ej. 24.1989,00
const SPD = ",";
const SPM = ".";

//simbolo de moneda
const SMONEY = "$";

?>