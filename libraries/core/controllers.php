<?php

class controllers{

    public function __construct(){

        $this->views = new views();
        $this->loadModel();

    }

    public function loadModel()
    {
        //HomeModel
        $model = get_class($this)."Model";
        $routclass = "models/".$model.".php";
        if(file_exists($routclass)){
            require_once($routclass);
            $this->model = new $model();
        }
    }

}

?>