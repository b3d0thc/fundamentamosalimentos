<?php

class Mysql extends conexion{
    private $conexion;
    private $strquery;
    private $arrvalues;

    function __construct(){
        $this->conexion = new conexion();
        $this->conexion = $this->conexion->conect();
    }

    //inserta un registro
    function insert(string $query, array $arrvalues){
        $this->strquery = $query;
        $this->arrvalues = $arrvalues;

        $insert = $this->conexion->prepare($this->strquery);
        $resinsert = $insert->execute($this->arrvalues);
        if($resinsert){
            $lastinsert = $this->conexion->lastInsertId();
        }else{
            $lastinsert = 0;
        }
        return $lastinsert;

    }

    //captura un registro
    public function select(string $query){
        $this->strquery = $query;
        $result = $this->conexion->prepare($this->strquery);
        $result->execute();
        $data = $result->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    //captura todo los registros
    public function select_all(string $query){
        $this->strquery = $query;
        $result = $this->conexion->prepare($this->strquery);
        $result->execute();
        $data = $result->fetchall(PDO::FETCH_ASSOC);
        return $data;
    }

    //actualizar registros
    public function update(string $query, array $arrvalues){
        $this->strquery = $query;
        $this->arrvalues = $arrvalues;
        $update = $this->conexion->prepare($this->strquery);
        $resexecute = $update->execute($this->arrvalues);
        return $resexecute;
    }

    //eliminar registro
    public function delete(string $query){
        $this->strquery = $query;
        $result = $this->conexion->prepare($this->strquery);
        $del = $result->execute();
        return $del;
    }
}
?>