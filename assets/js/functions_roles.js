
var tableRoles;

document.addEventListener('DOMContentLOaded', function(){

    tableRoles = $('#tableRoles').DataTable( {
           "aProcessing":true,
           "aServerSide":true,
           "lenguage":{
               "url": "//cdn.datables.net/plug-ins/1.10.20/i18n/Spainsh.json"
           },
            "ajax":{ 
                "url": " "+base_url+"/roles/getRoles",
                "dataSrc":""  },
            "columns": [
                {"data": "idrol"},
                {"data": "namerol"},
                {"data": "descripcion"},
                {"data": "status"},
            ],
            "resonsieve":"true",
            "bDestroy": true,
            "iDisplayLength": 10,
            "order":[[0,"desc"]]
        } );
    } );

$('#tableRoles').DataTable();
    
function openModal(){
    $('#modalFormRol').modal('show');
}