<?php 

require_once("autoload.php");

class usuario extends conexion{
    private $doc;
    private $nombre;
    private $direc;
    private $cllr;
    private $crro;
    private $pass;
    private $descrip;
    private $conexion;

 function __construct(){
     $this->conexion = new conexion();
     $this->conexion = $this->conexion->connect();
    }

    function insertuser(int $docu, string $name, string $dire, int $celu, string $correo, string $contra, string $descri){
        $this->doc = $docu;
        $this->nombre = $name;
        $this->direc = $dire;
        $this->cllr = $celu;
        $this->crro = $correo;
        $this->pass = $contra;
        $this->descrip = $descri;

        $sql ="INSERT INTO usuario(documento,nombre,direccion,Celular,correo,contra,descripcion)VALUES(?,?,?,?,?,?,?)";
        $insert = $this->conexion->prepare($sql);
        $arrdata = array($this->doc,$this->nombre,$this->direc,$this->cllr,$this->crro,$this->pass,$this->descrip);
        $resinsert = $insert->execute($arrdata);
        $idinsert = $this->conexion->lastInsertId();
        return $idinsert;

    }

    public function getusers(){
        $sql = "SELECT * FROM usuario";
        $execute = $this->conexion->query($sql);
        $request = $execute->fetchall(PDO::FETCH_ASSOC);
        return $request;

    }

    public function updateuser(int $doc, string $name, string $dire, int $celu, string $correo, string $contra, string $descri){
        $this->nombre = $name;
        $this->direc = $dire;
        $this->cllr = $celu;
        $this->crro = $correo;
        $this->pass = $contra;
        $this->descrip = $descri;

        $sql = "UPDATE usuario SET nombre=?, direccion=?, Celular=?, correo=?, contra=?, descripcion=? WHERE documento=$doc";
        $update = $this->conexion->prepare($sql);
        $arrdata = array($this->nombre,$this->direc,$this->cllr,$this->crro,$this->pass,$this->descrip);
        $resexecute = $update->execute($arrdata);
        return $resexecute;
    }

    public function getuser(int $docu){
        $sql = "SELECT * FROM usuario WHERE documento = ? ";
        $arrwhere = array($docu);
        $query = $this->conexion->prepare($sql);
        $query->execute($arrwhere);
        $request = $query->fetch(PDO::FETCH_ASSOC);
        return $request;
    }

    public function deluser(int $docu){
        $sql = "DELETE FROM usuario WHERE documento = ? ";
        $arrwhere = array($docu);
        $delete = $this->conexion->prepare($sql);
        $del = $delete->execute($arrwhere);
        return $del;
    }

}

?>