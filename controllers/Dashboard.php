<?php

    class Dashboard extends controllers{
        public function __construct(){

            parent::__construct();

        }

        public function dashboard(){
            $data['page_id'] = 2;
            $data['page_tag'] = "Dashboard - F.A-S.A.S";
            $data['page_title'] = "Dashboard - F.A-S.A.S";
            $data['page_name'] = "Dashboard";
            $this->views->getview($this,"dashboard",$data);
        }
        
        
    }

?>