<?php

    class Home extends controllers{
        public function __construct(){

            parent::__construct();

        }

        public function home(){
            $data['page_id'] = 1;
            $data['page_tag'] = "HOME - F.A-S.A.S";
            $data['page_title'] = "PAGINA INICIO";
            $data['page_name'] = "INICIO";
            $data['page_content'] = "Lorem, ipsum dolor sit amet consectetur adipisicing 
            elit. Voluptatibus alias corporis velit architecto nisi placeat vitae eveniet 
                            ullam illum aperiam recusandae 
            possimus molestias tempore iure corrupti, sit neque consectetur officiis.";
            $this->views->getview($this,"home",$data);
        }
        
        
    }

?>