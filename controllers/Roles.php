<?php

    class Roles extends controllers{
        public function __construct(){

            parent::__construct();

        }

        public function Roles(){
            $data['page_id'] = 3;
            $data['page_tag'] = "Roles Usuario";
            $data['page_name'] = "Rol_usuario";
            $data['page_title'] = "Roles Usuario";
            $this->views->getview($this,"roles",$data);
        }

        public function getRoles(){
            
            //Llamar al modals
            $arrData = $this->model->selectRoles();

            //Convertir una consulta de la db en formato json (Cualquier lenguaje)
            echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            die();
        }
        
    }

?>