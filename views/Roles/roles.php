<?php headeradmin($data); 
      getModal('modal_rol',$data);

?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="icon fab fa-r-project"></i> <?= $data['page_title'] ?> <button class="btn btn-link" type="button" onclick="openModal();" ><i class="fas fa-plus-square" ></i></button></h1>
          
          <p></p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="<?= base_url(); ?>/roles"><?= $data['page_title'] ?></a></li>
        </ul>
      </div>
      <!--<div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">Roles de usuarios</div>
          </div>
        </div>
      </div>-->

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="tableRoles">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Status</th>
                      <th><i class="app-menu__icon fas fa-tools"></i>Tools</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </main>
<?php footeradmin($data); ?>