<footer>
    <div class="row rds">
      <div class="col-sm-4 d-flex flex-column redes">
        <a href=""><i class="fab fa-facebook-square"></i> </a>
        <a href=""><i class="fab fa-google-plus-square"></i> </a>
        <a href=""><i class="fab fa-twitter-square"> </i></a>
        <a href=""><i class="fab fa-instagram-square"> </i></a>
      </div>

      <div class="col-sm-4">
        <address>
        <h3><span class="oi oi-briefcase"></span> OFICINA CENTRAL</h3>
        <p><span class="oi oi-location"></span> COLOMBIA, MEDELLIN</p>
        <p><span class="oi oi-home"></span> CR 120 #39a 20</p>
        <p><span class="oi oi-phone"></span> +57 300 915 2444</p>
        </address>
      </div>

      <div class="col-sm-4 d-flex flex-column redes align-items-end">
        <a href="" type="button" class="btn btn-link tm-1" data-toggle="modal" data-target="#contacto">AGREGAR DATOS</a>
        <a href="">NOSOTROS</a>
        <a href="">VISION & Vision</a>
        <a href="">Terminos & Condicones</a>
      </div>
    </div>
</footer>

<div class="autor">
    <i class="far fa-copyright"></i> autor: B3D0 @BEDOYATHC
</div>
    
    <!--scripts-->
<script type="text/javascript" src="<?= media(); ?>/js/jquery.min.js"></script> 
<script src="<?= media(); ?>/js/fontawesome.js"></script>
<script type="text/javascript" src="<?= media(); ?>/js/funtions.js"></script> 
<script type="text/javascript" src="<?= media(); ?>/js/bootstrap.min.js"></script>

</body>
</html>