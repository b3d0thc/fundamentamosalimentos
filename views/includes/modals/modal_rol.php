<!-- Modal -->
<div class="modal fade" id="modalFormRol" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Nuevo Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <div class="tile">
            <div class="tile-body">
              <form id="formRol" name="formRol">
                <div class="form-group">
                  <label class="control-label">Nombre</label>
                  <input class="form-control" id="txtname" name="txtname" type="text" placeholder="Nombre del Rol" require="">
                </div>
                
                <div class="form-group">
                  <label class="control-label">Descripcion</label>
                  <textarea class="form-control" id="txtdesp" name="txtdesp" rows="2" placeholder="Descripcion del Rol" require=""></textarea>
                </div>

                <div class="form-group">
                <label class="control-label">Estado</label>
                <select class="form-control" id="exampleSelect1">
                      <option value="1">activo</option>
                      <option calue="2">inactivo</option>
                      
                    </select>
                </div>
                <div class="tile-footer">
                <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>GUARDAR</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>CERRAR</a>
                </div>
              </form>
            </div>

          </div>
      </div>
    </div>
  </div>
</div>