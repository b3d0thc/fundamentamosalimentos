<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="<?= media(); ?>/images/avatarh.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">B3D0THC</p>
          <p class="app-sidebar__user-designation">administrador</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="<?= base_url(); ?>/dashboard"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">DASHBOARD</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">GESTOR USUARIOS</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?= base_url(); ?>/usuarios"><i class="app-menu__icon fas fa-users-cog"></i></i>USUARIOS</a></li>
            <li><a class="treeview-item" href="<?= base_url(); ?>/roles"><i class="app-menu__icon fab fa-r-project"></i>ROLES</a></li>
            <li><a class="treeview-item" href="<?= base_url(); ?>/permisos"><i class="app-menu__icon fas fa-user-edit"></i>PERMISOS</a></li>
          </ul>
        </li>
        
        <li><a class="app-menu__item" href="<?= base_url(); ?>/clientes"><i class="app-menu__icon far fa-id-badge"></i><span class="app-menu__label">CLIENTES</span></a></li>
        
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">PROGRAMAS</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="<?= base_url(); ?>/usuarios"><i class="app-menu__icon fas fa-book-reader"></i></i>CURSOS</a></li>
            <li><a class="treeview-item" href="<?= base_url(); ?>/roles"><i class="app-menu__icon fas fa-concierge-bell"></i>SERVICIOS</a></li>
          </ul>
        </li>

        <li><a class="app-menu__item" href="<?= base_url(); ?>"><i class="app-menu__icon fa fa-building-o"></i><span class="app-menu__label">EMPRESAS</span></a></li>
        <li><a class="app-menu__item" href="<?= base_url(); ?>"><i class="app-menu__icon fa fa-home"></i><span class="app-menu__label">INICIO</span></a></li>

        <li><a class="app-menu__item" href="<?= base_url(); ?>/login"><i class="app-menu__icon fa fa-sign-out"></i><span class="app-menu__label">CERRARSESION</span></a></li>

      </ul>
    </aside>