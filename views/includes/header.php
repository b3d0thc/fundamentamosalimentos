<?php require_once("config/config.php");
    require_once("helpers/helpers.php");
    
     ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Empresa con cursos, asesorias y servicios">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="@BEDOYATHC">
    <meta name="theme-color" content="#96000c">
    <link rel="shortcut icon" href="<?= media(); ?>/images/favicon.ico">
    <title> <?= $data['page_tag'] ?> </title> 

        <!-- CDN Frameworks -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

        <!-- CSS propio -->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">

        <!-- FontAwesome -->
        <script src="<?= media(); ?>/js/fontawesome.js" crossorigin="anonymous"></script>

</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="container ">
        <a href="<?php echo base_url();?>" class="navbar-brand"><img src="<?php echo base_url();?>/assets/images/logo-fa1.png" alt="Logo" style="width: 80px;"> FUNDAMENTAMOS ALIMENTOS S.A.S</a>
        <button class="btntogle navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav me-auto mb-lg-0">
            <li class="nav-item"><a class="nav-link " arial-current="page" href="<?php echo base_url();?>">INICIO</a></li>
            <li class="nav-item"><a class="nav-link " arial-current="page" href="<?php echo base_url();?>/cursos">CURSOS</a></li>
            
            <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            NOSOTROS
          </a>
          <ul class="dropdown-menu c-b" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="<?php echo base_url();?>/qs">¿QUIENES SOMOS?</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url();?>/vym">VISION Y MISION</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url();?>/galeria">GALERIA</a></li>
          </ul>
        </li>

            </li>
            <li class="nav-item"><a class="nav-link " arial-current="page" href="<?php echo base_url();?>/registrar">REGISTRARME</a></li>
        </ul>
    </div>
</nav>
