<?php headeer($data); ?>

<div class="jumbotron">
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <ol class="carousel-indicators">
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="5"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="6"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="7"></li>
    <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="8"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="<?php echo base_url();?>/assets/images/uploads/1.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/2.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/3.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/4.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/5.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/6.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/7.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/8.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url();?>/assets/images/uploads/9.jpg" class="d-block w-100" alt="..." style="height: 400px">
      <div class="carousel-caption d-none d-md-block">
        <h5></h5>
        <p></p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </a>
</div>
</div>

<div class="container jumbotron-c ">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item cl" aria-current="page"><a href="<?php base_url(); ?>"><?php echo $data['page_tag']; ?></a></li>
        <li class="breadcrumb-item" aria-current="page"><?php echo $data['page_name']; ?></li>
          
        </ol>
      </nav>
    </div>

    <div class="d-flex flex-wrap">
        <div class="col mg1">
            <div class="card crd d-flex flex-column">
                <div>
                  <img src="<?php base_url(); ?>/assets/images/uploads/estudiaconn.png" class="card-img-top" height="200px" alt="...">
                    <div class="card-body">
                      
                      <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">INSCRÍBETE AHORA</h5>
                      <p class="card-text">Déjanos tus datos y te habilitaremos los talleres para que
                    los resuelvas y tengas tu certificado. (PUEDES PAGAR AL FINAL DEL EXAMEN)</p>
                        </div>
                        
                </div>
                <div>  
                  <a class="btn btn-go btn-warning" data-toggle="modal" data-target="#modalinfo" id="infobtn" title="Click para ver mas">
                    <span class="oi oi-account-login"></span>INSCRÍBERME</a>
                 </div>
            </div>
        </div>  

        <div class="col mg1">
                  <div class="card crd d-flex flex-column ">
                    <img src="<?php base_url(); ?>/assets/images/uploads/servi1.jpg" class="card-img-top" height="200px" alt="...">
                    <div class="card-body">
                      <div>
                      <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">PLAN DE SANEAMIENTO</h5>
                      <p class="card-text">Laboración e implementación del plan de saneamiento básico <br>
                               ¿Qué es una plan de saneamiento básico?</p>
                      
                  </div>
                </div>
                  <div>
                  <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas">
                  <span class="oi oi-account-login"></span> VER MÁS</a>
                  </div>
                </div>
          </div>


        <div class="col mg1">
            <div class="card crd d-flex flex-column">
                <img src="<?php base_url(); ?>/assets/images/uploads/servi2.jpg" class="card-img-top" height="200px" alt="...">
              <div class="card-body">
                <div>
                <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">ASESORÍA Y DIAGNÓSTICO</h5>
                <p class="card-text">El paso previo antes de elaborar e implementar un plan de saneamiento 
                    básico es realizar una asesoría y un diagnóstico sanitario.</p>
                  </div>
              
                
                </div>
                <div>
                  <a class="btn btn-go btn-warning" data-toggle="modal" data-target="#modalinfo" id="infobtn" title="Click para ver mas">       
                    <span class="oi oi-account-login"></span> VER MÁS</a>
                </div>
              </div>
            </div>
 

  <div class="col mg1">
    <div class="card crd d-flex flex-column">
      <img src="<?php base_url(); ?>/assets/images/uploads/servi3.jpg" class="card-img-top" height="200px" alt="...">
      <div class="card-body">
        <div>
        <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">MINUTAS ALIMENTARIAS</h5>
        <p class="card-text">Fundamentamos cuenta además con el servicio de elaboración de minutas alimentarias</p>
         </div>
      </div>
      <div>
        <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas"
        ><span class="oi oi-account-login"></span> VER MÁS</a>
      </div>
    </div>
</div>
<div class="col mg1">
    <div class="card crd d-flex flex-column ">
      <img src="<?php base_url(); ?>/assets/images/uploads/cur1.jpg" class="card-img-top" height="200px" alt="...">
      <div class="card-body">
        <div>
        <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">CURSO DE MANIPULACION DE ALIMENTOS</h5>
        <p class="card-text">Somos una empresa líder en la formación y capacitación en manipulación de alimentos...</p>
              </div>
      </div>
      <div>
        <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas"
        ><span class="oi oi-account-login"></span> VER MÁS</a></div>
    </div>
</div>
<div class="col mg1">
    <div class="card crd d-flex flex-column">

      <img src="<?php base_url(); ?>/assets/images/uploads/cur2.jpg" class="card-img-top" height="200px" alt="...">
      <div class="card-body">
        <div>
        <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="SEGURIDAD DE LOS DATOS">CURSO BÁSICO DE PRIMEROS AUXILIOS</h5>
        <p class="card-text">Identificar e implementar la clasificación de residuos sólidos hospitalarios; también considerados de riesgo biológico...</p>
                      </div>
      </div>
      <div>
        <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas"
        ><span class="oi oi-account-login"></span> VER MÁS</a>
       </div>
    </div>
</div>
<div class="col mg1">
  <div class="card crd d-flex flex-column ">
    <img src="<?php base_url(); ?>/assets/images/uploads/cur3.jpg" class="card-img-top" height="200px" alt="...">
    <div class="card-body">
      <div>
      <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">CURSO BÁSICO Y ESPECIALIZADO EN ASEO HOSPITALARIO</h5>
      <p class="card-text">Desarrollar en el participante las competencias necesarias para brindar una atención
                 inicial e inmediata en pacientes en situación de urgencia y emergencia...</p>
      
  </div>
</div>
  <div>
  <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas"
  ><span class="oi oi-account-login"></span> VER MÁS</a>
  </div>
</div>
</div>
<div class="col mg1">
  <div class="card crd d-flex flex-column">
    <img src="<?php base_url(); ?>/assets/images/uploads/cur4.jpg" class="card-img-top" height="200px" alt="...">
    <div class="card-body">
      <div>
      <h5 class="card-title" data-toggle="tooltip" data-placement="top" title="">CURSOS DE BIOSEGURIDAD</h5>
      <p class="card-text">Cursos de bioseguridad en este curso se presentan los conceptos generales relacionados con el tema de la salud y el trabajo...</p>
       </div>
    </div>
    <div>
      <a href="#" class="btn btn-go btn-warning" data-toggle="popover" title="Click para ver mas"
      ><span class="oi oi-account-login"></span> VER MÁS</a>
    </div>
  </div>
</div>
</div>

<section id="<?php echo $data['page_id']; ?>">
    <h1><?php echo $data['page_title']; ?></h1>
    <p><?php echo $data['page_content']; ?></p><br>
</section>

    <?php echo base_url(); ?><br>
    <?php echo passgenerator(); ?><br>
    <?php echo token(); ?><br>
    <?php echo SMONEY.formatmoney(10000010); ?>
    

<?php footer($data); ?>